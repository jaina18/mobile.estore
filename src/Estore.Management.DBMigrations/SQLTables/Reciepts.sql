/****** Object:  Table [dbo].[Reciepts]    Script Date: 03-10-2021 14:50:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Reciepts](
	[RecieptId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RetailerId] [numeric](18, 0) NOT NULL,
	[RecieptNumber] [varchar](50) NOT NULL,
	[Amount] [numeric](18, 0) NOT NULL,
	[RecieptDate] [datetime2](7) NOT NULL,
	[Archived] [bit] NOT NULL,
 CONSTRAINT [PK_Reciepts] PRIMARY KEY CLUSTERED 
(
	[RecieptId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Reciepts]  WITH CHECK ADD  CONSTRAINT [FK_Reciepts_Retailers] FOREIGN KEY([RetailerId])
REFERENCES [dbo].[Retailers] ([RetailerId])
GO

ALTER TABLE [dbo].[Reciepts] CHECK CONSTRAINT [FK_Reciepts_Retailers]
GO


