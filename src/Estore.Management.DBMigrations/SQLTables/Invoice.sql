/****** Object:  Table [dbo].[Invoices]    Script Date: 03-10-2021 14:48:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Invoices](
	[InvoiceId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RetailerId] [numeric](18, 0) NOT NULL,
	[InvoiceNumber] [varchar](50) NOT NULL,
	[Amount] [numeric](18, 0) NOT NULL,
	[InvoiceDate] [datetime2](7) NOT NULL,
	[Archived] [bit] NOT NULL,
 CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED 
(
	[InvoiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_Invoices] FOREIGN KEY([RetailerId])
REFERENCES [dbo].[Retailers] ([RetailerId])
GO

ALTER TABLE [dbo].[Invoices] CHECK CONSTRAINT [FK_Invoices_Invoices]
GO


