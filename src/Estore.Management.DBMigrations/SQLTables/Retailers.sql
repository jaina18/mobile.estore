/****** Object:  Table [dbo].[Retailers]    Script Date: 03-10-2021 14:51:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Retailers](
	[RetailerId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[NickName] [varchar](100) NULL,
	[Address] [varchar](100) NULL,
	[CityId] [int] NOT NULL,
	[MobileNumber] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[Archived] [bit] NOT NULL,
 CONSTRAINT [PK_Retailers] PRIMARY KEY CLUSTERED 
(
	[RetailerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


