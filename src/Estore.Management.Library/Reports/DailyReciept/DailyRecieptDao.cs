﻿using Estore.Management.Library.Shared;

namespace Estore.Management.Library.Reports
{
    public sealed class DailyRecieptDao : InfoDao
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal RecieptId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RetailerName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RecieptNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
    }
}
