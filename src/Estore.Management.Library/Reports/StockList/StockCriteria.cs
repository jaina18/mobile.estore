﻿using System;

namespace Estore.Management.Library.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class StockCriteria
    {
        /// <summary>
        /// 
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? BrandTypeId { get; set; }
    }
}
