﻿using System;
using System.Collections.Generic;

namespace Estore.Management.Library.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class StockDao
    {
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime StockDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<StockCatelogDao> Catelogs { get; set; }
    }
}
