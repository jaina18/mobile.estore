﻿using Estore.Management.DataAccess.Reports;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Estore.Management.Library.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class StockReport : IReport<List<StockDao>, StockCriteria>
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IStockDal dal;

        /// <summary>
        /// 
        /// </summary>
        public StockReport()
        {
            this.dal = new StockDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async Task<List<StockDao>> Get(StockCriteria criteria)
        {
            var daos = new List<StockDao>();
            var dtos = await dal.FetchList(criteria.Year, criteria.Month, criteria.BrandTypeId);
            dtos.ForEach(item => daos.Add(new StockDao()
            {
                StockDate = item.StockDate,
                Catelogs = item.Catelogs.Select(catelog => new StockCatelogDao() 
                {
                    BrandTypeId = catelog.BrandTypeId,
                    Name = DataAccess.Brands.BrandDal.GetName(catelog.BrandTypeId) + " " + catelog.Name,
                    Quantity = catelog.Quantity
                }).ToList()
            }));
            return daos;
        }
    }
}
