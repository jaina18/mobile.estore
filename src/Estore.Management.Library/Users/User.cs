﻿using Estore.Management.DataAccess.Users;
using System.Threading.Tasks;

namespace Estore.Management.Library.Users
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class User : IUser
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUserDal userDal;

        /// <summary>
        /// 
        /// </summary>
        public User()
        {
            userDal = new UserDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<UserDao> Get(string emailAddress, string password)
        {
            var dto = await userDal.Fetch(emailAddress, password);
            if(dto != null)
            {
                return new UserDao()
                {
                    Active = dto.Active,
                    EmailAddress = dto.EmailAddress,
                    EmailVerified = dto.EmailVerified,
                    ErrorMessage = string.Empty,
                    Id = dto.Id,
                    LastLogin = dto.LastLogin,
                    Name = dto.Name,
                    Status = true
                };
            }
            else
            {
                return new UserDao()
                {
                    Status = false,
                    ErrorMessage = "Invalid credentials, please try again"
                };
            }
        }
    }
}
