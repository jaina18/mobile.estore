﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.Library.PurchaseOrders
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPurchaseOrder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchaserOrderId"></param>
        /// <returns></returns>
        Task<PurchaseOrderDao> Get(decimal purchaserOrderId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task New(PurchaseOrderDao dao);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchaseOrderId"></param>
        /// <returns></returns>
        Task Remove(decimal purchaseOrderId);
    }
}
