﻿using System;

namespace Estore.Management.Library.PurchaseOrders
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class PurchaseOrderDao
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal PurchaseOrderId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal CatelogId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CatelogDisplayName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Archived { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime OrderDate { get; set; }
    }
}
