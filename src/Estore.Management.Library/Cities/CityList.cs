﻿using Estore.Management.DataAccess.Cities;
using System.Collections.Generic;

namespace Estore.Management.Library.Cities
{
    public sealed class CityList
    {
        /// <summary>
        /// 
        /// </summary>
        public CityList()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<CityDao> Get()
        {
            var daos = new List<CityDao>();
            foreach(var item in CityDal.Get())
            {
                daos.Add(new CityDao(item.Id, item.Name));
            }
            return daos;
        }
    }
}
