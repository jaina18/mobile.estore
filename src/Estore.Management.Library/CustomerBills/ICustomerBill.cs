﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.Library.CustomerBills
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICustomerBill
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        Task New(CustomerBillDao dao);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerBillId"></param>
        Task Delete(decimal customerBillId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        Task<CustomerBillDao> Fetch(decimal invoiceId);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceDate"></param>
        /// <returns></returns>
        Task<List<CustomerBillDao>> FetchList(string invoiceDate);
    }
}
