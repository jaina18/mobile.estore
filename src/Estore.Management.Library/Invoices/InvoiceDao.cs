﻿using Estore.Management.Library.Shared;
using System;
using System.Collections.Generic;

namespace Estore.Management.Library.Invoices
{
    public sealed class InvoiceDao : EditDao
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal InvoiceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RetailerName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? InvoiceDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Archived { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsCashTransaction { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RecieptNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<InvoiceLineDao> InvoiceLines { get; set; }
    }
}
