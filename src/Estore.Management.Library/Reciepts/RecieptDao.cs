﻿using Estore.Management.Library.Shared;

namespace Estore.Management.Library.Reciepts
{
    public sealed class RecieptDao : EditDao
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal RecieptId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RecieptNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.DateTime RecieptDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Archived { get; set; }
    }
}
