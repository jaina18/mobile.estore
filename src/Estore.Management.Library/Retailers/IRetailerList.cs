﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.Library.Retailers
{
    public interface IRetailerList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="queryExpression"></param>
        /// <param name="archived"></param>
        /// <returns></returns>
        Task<List<RetailerInfoDao>> Get(int cityId, string queryExpression, bool? archived = null);
    }
}
