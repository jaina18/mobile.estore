﻿using System;

namespace Estore.Management.Library.Retailers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class TransactionDao
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal TransactionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TransactionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BillNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime TransactionDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsCashTransaction { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RecieptNumber { get; set; }
    }
}
