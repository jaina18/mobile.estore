﻿using Estore.Management.Library.Shared;

namespace Estore.Management.Library.Retailers
{
    public sealed class RetailerDao : EditDao
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MobileNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Archived { get; set; }
    }
}
