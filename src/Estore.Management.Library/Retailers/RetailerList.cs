﻿using Estore.Management.DataAccess.Retailers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.Library.Retailers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RetailerList : IRetailerList
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IRetailerDal dal;

        public RetailerList()
        {
            dal = new RetailerDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="queryExpression"></param>
        /// <param name="archived"></param>
        /// <returns></returns>
        public async Task<List<RetailerInfoDao>> Get(int cityId, string queryExpression, bool? archived = null)
        {
            var dtos = await dal.FetchList(cityId, queryExpression, archived);
            return dtos.Select(item => new RetailerInfoDao()
            {
                Address = item.Address,
                City = item.City,
                MobileNumber = item.MobileNumber,
                Name = item.Name,
                NickName = item.NickName,
                RetailerId = item.RetailerId,
                Status = true,
                BilledAmount = item.BilledAmount,
                CollectedAmount = item.CollectedAmount,
                ErrorMessage = string.Empty,
                LastInvoiceAmount = item.LastInvoiceAmount,
                LastInvoiceDate = item.LastInvoiceDate,
                LastRecieptDate = item.LastRecieptDate,
                LastRecieptAmount = item.LastRecieptAmount
            }).ToList();
        }
    }
}
