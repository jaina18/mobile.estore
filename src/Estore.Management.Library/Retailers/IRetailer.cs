﻿using System.Threading.Tasks;

namespace Estore.Management.Library.Retailers
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRetailer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task<RetailerDao> Get(decimal retailerId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task<RetailerInfoDao> GetInfo(decimal retailerId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task New(RetailerDao dao);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task Save(RetailerDao dao);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task Delete(RetailerDao dao);
    }
}
