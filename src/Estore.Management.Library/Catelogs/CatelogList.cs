﻿using Estore.Management.DataAccess.Catelogs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.Library.Catelogs
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CatelogList : ICatelogList
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICatelogDal dal;

        /// <summary>
        /// 
        /// </summary>
        public CatelogList()
        {
            this.dal = new CatelogDal();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandTypeId"></param>
        /// <returns></returns>
        public async Task<List<CatelogInfoDao>> Get(int? brandTypeId = null)
        {
            var daos = new List<CatelogInfoDao>();
            var dtos = await dal.FetchList(brandTypeId);
            dtos.ForEach(item => daos.Add(new CatelogInfoDao()
            {
                BrandTypeId = item.BrandTypeId,
                BrandName = item.BrandName,
                CatelogId = item.CatelogId,
                CreatedDate = item.CreatedDate,
                Memory = item.Memory,
                Name = item.Name,
                RAM = item.RAM,
                LastOrderDateTime = item.LastOrderDateTime,
                Quantity = item.Quantity,
                LastOrderQuantity = item.LastOrderQuantity,
                Active = item.Active
            }));
            return daos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        public async Task<List<SelectedDateCatelogInfoDao>> Get(DateTime dateTime, decimal catelogId)
        {
            var daos = new List<SelectedDateCatelogInfoDao>();
            var dtos = await dal.FetchList(dateTime, catelogId);
            dtos.ForEach(item => daos.Add(new SelectedDateCatelogInfoDao()
            {
                InvoiceNumber = item.InvoiceNumber,
                Name = item.Name + ", " + DataAccess.Cities.CityDal.GetName(item.CityId),
                Quantity = item.Quantity
            }));
            return daos;
        }
    }
}
