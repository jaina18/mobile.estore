﻿using System.Threading.Tasks;

namespace Estore.Management.Library.Catelogs
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICatelog
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        Task<CatelogDao> Get(decimal catelogId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task New(CatelogDao dao);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task Save(CatelogDao dao);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        Task Status(decimal catelogId);
    }
}
