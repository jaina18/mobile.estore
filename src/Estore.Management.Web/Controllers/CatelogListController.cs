﻿using Estore.Management.Library.Catelogs;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Mobile.Estore.Web.Controllers
{
    [Authorize]
    public class CatelogListController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICatelogList catelogList;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICatelog catelog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="billOrder"></param>
        /// <param name="billOrderDal"></param>
        public CatelogListController()
        {
            catelogList = new CatelogList();
            catelog = new Catelog();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(int? brandTypeId = null)
        {
            ViewBag.BrandTypeId = brandTypeId != null ? brandTypeId : -1;
            var model = await catelogList.Get(brandTypeId);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Save(CatelogDao dao)
        {
            if(dao.CatelogId == 0)
            {
                await catelog.New(dao);
            }
            else
            {
                await catelog.Save(dao);
            }
            return RedirectToAction("Index", new { @brandTypeId = dao.BrandTypeId });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandTypeId"></param>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Update(decimal catelogId, int? brandTypeId = null)
        {
            await catelog.Status(catelogId);
            return RedirectToAction("Index", new { brandTypeId });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<JsonResult> Get(decimal catelogId)
        {
            var model = await catelog.Get(catelogId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedDate"></param>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<JsonResult> Search(DateTime selectedDate, decimal catelogId = 0)
        {
            var model = await catelogList.Get(selectedDate, catelogId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}