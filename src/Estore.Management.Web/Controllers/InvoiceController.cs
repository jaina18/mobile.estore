﻿using Estore.Management.Library.Catelogs;
using Estore.Management.Library.Invoices;
using Estore.Management.Library.Reciepts;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Mobile.Estore.Web.Controllers
{
    [Authorize]
    public class InvoiceController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IInvoice invoice;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICatelogList catelogList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IReciept Reciept;

        /// <summary>
        /// 
        /// </summary>
        public InvoiceController()
        {
            this.invoice = new Invoice();
            this.catelogList = new CatelogList();
            this.Reciept = new Reciept();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<JsonResult> Get(decimal invoiceId)
        {
            var model = await invoice.Get(invoiceId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retailerId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> New(decimal retailerId, string retailerName)
        {
            ViewBag.CateLogList = await catelogList.Get();
            var model = new InvoiceDao()
            {
                RetailerName = retailerName,
                RetailerId = retailerId,
                InvoiceLines = new System.Collections.Generic.List<InvoiceLineDao>()
                {
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 1,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 2,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 3,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 4,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 5,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 6,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 7,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 8,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 9,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 10,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 11,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 12,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 13,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 14,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 15,
                        Quantity = 1
                    },
                    new InvoiceLineDao()
                    {
                        LineId = 0,
                        LineNumber = 16,
                        Quantity = 1
                    }
                },
                Archived = false,
                IsCashTransaction = false,
                Discount = 0
            };
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> New(InvoiceDao model)
        {
            await invoice.New(model);
            await Task.Delay(1000);
            if(model.Status)
            {
                if(model.IsCashTransaction)
                {
                    var dao = new RecieptDao()
                    {
                        Amount = model.InvoiceLines.Where(x => x.Archived).Sum(x => x.Price * x.Quantity) - model.Discount,
                        RetailerId = model.RetailerId,
                        RecieptNumber = model.RecieptNumber,
                        RecieptDate = model.InvoiceDate.Value
                    };
                    await Reciept.New(dao);
                }
                return RedirectToAction("Index", "RetailerInfo", new { @retailerId = model.RetailerId });
            }
            ViewBag.RetailerId = model.RetailerId;
            ViewBag.CateLogList = await catelogList.Get();
            return View(model);
        }
    }
}