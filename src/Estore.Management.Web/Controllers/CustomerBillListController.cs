﻿using Estore.Management.Library.CustomerBills;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Mobile.Estore.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class CustomerBillListController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICustomerBill customerBill;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="billOrder"></param>
        /// <param name="billOrderDal"></param>
        public CustomerBillListController()
        {
            customerBill = new CustomerBill();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index(string invoiceDate)
        {
            ViewBag.InvoiceDate = invoiceDate;
            var list = await customerBill.FetchList(invoiceDate);
            return View(list);
        }
    }
}