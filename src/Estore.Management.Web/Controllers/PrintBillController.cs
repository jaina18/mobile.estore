﻿using Estore.Management.Library.CustomerBills;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Mobile.Estore.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class PrintBillController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICustomerBill customerBill;

        /// <summary>
        /// 
        /// </summary>
        public PrintBillController()
        {
            customerBill = new CustomerBill();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal invoiceId)
        {
            var dao = await customerBill.Fetch(invoiceId);
            return View(dao);
        }
    }
}