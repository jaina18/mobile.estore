﻿using Estore.Management.Library.Users;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace Mobile.Estore.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private new readonly IUser User;

        /// <summary>
        /// 
        /// </summary>
        public HomeController()
        {
            User = new User();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            await Task.FromResult(1);
            var model = new UserDao()
            {
                Status = true,
                ErrorMessage = string.Empty
            };
            return View(model);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Logout()
        {
            await Task.FromResult(1);
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(UserDao dao)
        {
            dao = await User.Get(dao.EmailAddress, dao.Password);
            if (dao.Active)
            {
                var cookiesDetail = dao.Name + "~" + dao.EmailAddress;
                FormsAuthentication.SetAuthCookie(cookiesDetail, false);
                return RedirectToAction("Index", "CustomerBillList", new { @invoiceDate = DateTime.Now.ToString("yyyy-MM-dd") });
            }
            return View(dao);
        }
    }
}