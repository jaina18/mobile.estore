﻿using Estore.Management.Library.Catelogs;
using Estore.Management.Library.CustomerBills;
using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Mobile.Estore.Web.Controllers
{
    [Authorize]
    public class CustomerBillEditController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ICustomerBill customerBill;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICatelogList catelogList;

        /// <summary>
        /// 
        /// </summary>
        public CustomerBillEditController()
        {
            customerBill = new CustomerBill();
            catelogList = new CatelogList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceDate"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(string invoiceDate)
        {
            await Task.FromResult(1);
            ViewBag.ActiveMenu = "NewCustomerBill";
            ViewBag.CateLogList = await catelogList.Get();
            var model = new CustomerBillDao()
            {
                ModelNumber = string.Empty,
                InvoiceDate = invoiceDate
            };
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Index(CustomerBillDao dao)
        {
            ViewBag.ActiveMenu = "NewCustomerBill";
            dao.BillDateTime = DateTime.ParseExact(dao.InvoiceDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            await customerBill.New(dao);
            if (dao.Status)
            {
                return RedirectToAction("Index", "CustomerBillList", new { @invoiceDate = dao.InvoiceDate });
            }
            ViewBag.CateLogList = await catelogList.Get();
            return View(dao);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Delete(decimal invoiceId, string invoiceDate)
        {
            await customerBill.Delete(invoiceId);
            return RedirectToAction("Index", "CustomerBillList", new { @invoiceDate = invoiceDate });
        }
    }
}