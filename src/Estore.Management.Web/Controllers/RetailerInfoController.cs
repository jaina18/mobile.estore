﻿using Estore.Management.Library.Retailers;
using Estore.Management.Library.Reciepts;
using Estore.Management.Library.Invoices;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Mobile.Estore.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class RetailerInfoController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IRetailer Retailer;

        /// <summary>
        /// 
        /// </summary>
        private readonly IInvoice Invoice;

        /// <summary>
        /// 
        /// </summary>
        private readonly IReciept Reciept;

        /// <summary>
        /// 
        /// </summary>
        public RetailerInfoController()
        {
            this.Retailer = new Retailer();
            this.Invoice = new Invoice();
            this.Reciept = new Reciept();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Index(decimal retailerId)
        {
            ViewBag.RetailerId = retailerId;
            var model = await Retailer.GetInfo(retailerId);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> NewTransaction(TransactionDao dao)
        {
            if(dao.TransactionType.Contains("Cash"))
            {
                var recieptDao = new RecieptDao()
                {
                    Amount = dao.Amount,
                    RetailerId = dao.RetailerId,
                    RecieptDate = dao.TransactionDate,
                    RecieptNumber = dao.BillNumber
                };
                await this.Reciept.New(recieptDao);
            }
            if(dao.TransactionType.Contains("Bill"))
            {
                var invoiceDao = new InvoiceDao()
                {
                    Amount = dao.Amount,
                    RetailerId = dao.RetailerId,
                    InvoiceDate = dao.TransactionDate,
                    InvoiceNumber = dao.BillNumber
                };
                await this.Invoice.New(invoiceDao);

                if(dao.IsCashTransaction)
                {
                    var recieptDao = new RecieptDao()
                    {
                        Amount = dao.Amount,
                        RetailerId = dao.RetailerId,
                        RecieptDate = dao.TransactionDate,
                        RecieptNumber = dao.RecieptNumber
                    };
                    await this.Reciept.New(recieptDao);
                }
            }
            return RedirectToAction("Index", new { @retailerId = dao.RetailerId });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retailerId"></param>
        /// <param name="transactionId"></param>
        /// <param name="transactionType"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> DeleteTransaction(decimal retailerId, decimal transactionId, string transactionType)
        {
            if (retailerId > 0 && transactionId > 0 && transactionType != null)
            {
                if (transactionType.Contains("Reciept"))
                {
                    await this.Reciept.Delete(new RecieptDao() { RecieptId = transactionId });
                }
                else if (transactionType.Contains("Invoice"))
                {
                    await this.Invoice.Delete(new InvoiceDao() { InvoiceId = transactionId });
                }
            }
            return RedirectToAction("Index", new { @retailerId = retailerId });
        }
    }
}