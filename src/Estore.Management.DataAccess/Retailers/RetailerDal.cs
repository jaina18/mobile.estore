﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Retailers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RetailerDal : IRetailerDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="archived"></param>
        /// <param name="cityId"></param>
        /// <param name="queryExpression"></param>
        /// <returns></returns>
        public async Task<List<RetailerInfoDto>> FetchList(int cityId, string queryExpression, bool? archived = null)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var query = (from c in ctx.Retailers
                             where c.Archived == false
                             orderby c.Name
                             select new
                             {
                                 c.Active,
                                 c.Address,
                                 c.CityId,
                                 c.MobileNumber,
                                 c.Name,
                                 c.NickName,
                                 c.RetailerId,
                                 c.Archived,
                                 BilledAmount = c.Invoices.Count > 0 ? c.Invoices.Sum(x => x.Amount) : 0,
                                 CollectedAmount = c.Reciepts.Count > 0 ? c.Reciepts.Sum(x => x.Amount) : 0,

                                 LastInvoiceDate = c.Invoices.Count > 0 ? c.Invoices.OrderByDescending(x => x.InvoiceDate).FirstOrDefault().InvoiceDate : DateTime.MinValue,
                                 LastInvoiceAmount = c.Invoices.Count > 0 ? c.Invoices.OrderByDescending(x => x.InvoiceDate).FirstOrDefault().Amount : 0,
                                 
                                 LastRecieptDate = c.Reciepts.Count > 0 ? c.Reciepts.OrderByDescending(x => x.RecieptDate).FirstOrDefault().RecieptDate : DateTime.MinValue,
                                 LastRecieptAmount = c.Reciepts.Count > 0 ? c.Reciepts.OrderByDescending(x => x.RecieptDate).FirstOrDefault().Amount : 0,
                             });

                if (cityId > 0)
                {
                    query = query.Where(c => c.CityId == cityId);
                }
                if (!string.IsNullOrEmpty(queryExpression))
                {
                    query = query.Where(c => c.Name.Contains(queryExpression));
                }

                var data = (from c in query
                            select new RetailerInfoDto()
                            {
                                Address = c.Address,
                                CityId = c.CityId,
                                MobileNumber = c.MobileNumber,
                                Name = c.Name,
                                NickName = c.NickName,
                                RetailerId = c.RetailerId,
                                BilledAmount = c.BilledAmount,
                                CollectedAmount = c.CollectedAmount,
                                LastInvoiceDate = c.LastInvoiceDate,
                                LastInvoiceAmount = c.LastInvoiceAmount,
                                LastRecieptAmount = c.LastRecieptAmount,
                                LastRecieptDate = c.LastRecieptDate
                            }).ToList();

                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retailerId"></param>
        /// <returns></returns>
        public async Task<RetailerDto> Fetch(decimal retailerId)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Retailers
                            where c.RetailerId == retailerId
                            select c).SingleOrDefault();

                return new RetailerDto()
                {
                    Active = data.Active,
                    Address = data.Address,
                    Archived = data.Archived,
                    CityId = data.CityId,
                    MobileNumber = data.MobileNumber,
                    Name = data.Name,
                    NickName = data.NickName,
                    RetailerId = data.RetailerId
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retailerId"></param>
        /// <returns></returns>
        public async Task<RetailerInfoDto> FetchInfo(decimal retailerId)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Retailers
                            where c.RetailerId == retailerId
                            select new RetailerInfoDto()
                            {
                                Address = c.Address,
                                CityId = c.CityId,
                                MobileNumber = c.MobileNumber,
                                Name = c.Name,
                                NickName = c.NickName,
                                RetailerId = c.RetailerId,
                                BilledAmount = c.Invoices.Count() > 0 ? c.Invoices.Where(x => x.Archived == false).Sum(x => x.Amount) : 0,
                                CollectedAmount = c.Reciepts.Count() > 0 ? c.Reciepts.Where(x => x.Archived == false).Sum(x => x.Amount) : 0
                            }).SingleOrDefault();

                var invoices = ctx.Invoices.Where(r => r.RetailerId == retailerId && r.Archived == false).Select(item => new TransactionDto()
                {
                    Amount = item.Amount,
                    BillNumber = item.InvoiceNumber,
                    TransactionDate = item.InvoiceDate,
                    TransactionId = item.InvoiceId,
                    TransactionType = "Invoice"
                }).ToList();

                var reciepts = ctx.Reciepts.Where(r => r.RetailerId == retailerId && r.Archived == false).Select(item => new TransactionDto()
                {
                    Amount = item.Amount,
                    BillNumber = item.RecieptNumber,
                    TransactionDate = item.RecieptDate,
                    TransactionId = item.RecieptId,
                    TransactionType = "Reciept"
                }).ToList();

                data.Transactions.AddRange(invoices);
                data.Transactions.AddRange(reciepts);

                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(RetailerDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var newItem = new Retailer()
                {
                    Active = dto.Active,
                    Address = dto.Address,
                    Archived = dto.Archived,
                    CityId = dto.CityId,
                    MobileNumber = dto.MobileNumber,
                    Name = dto.Name,
                    NickName = dto.NickName,
                    RetailerId = 0
                };

                ctx.Retailers.Add(newItem);
                await ctx.SaveChangesAsync();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(RetailerDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Retailers
                            where c.RetailerId == dto.RetailerId
                            select c).SingleOrDefault();

                data.Active = dto.Active;
                data.Address = dto.Address;
                data.Archived = dto.Archived;
                data.CityId = dto.CityId;
                data.MobileNumber = dto.MobileNumber;
                data.Name = dto.Name;
                data.NickName = dto.NickName;

                await ctx.SaveChangesAsync();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Delete(decimal retailerId)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Retailers
                            where c.RetailerId == retailerId
                            select c).SingleOrDefault();

                data.Archived = true;
                await ctx.SaveChangesAsync();
            }
        }
    }
}
