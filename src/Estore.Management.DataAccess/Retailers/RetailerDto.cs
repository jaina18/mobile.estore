﻿using System;

namespace Estore.Management.DataAccess.Retailers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RetailerDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string NickName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int CityId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string MobileNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool Active { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool Archived { get; set; }
    }
}
