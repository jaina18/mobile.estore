﻿using System;

namespace Estore.Management.DataAccess.InvoiceLines
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class InvoiceLineDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal LineId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int LineNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal InvoiceId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal CatelogId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal Price { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int Quantity { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool? Archived { get; set; }
    }
}
