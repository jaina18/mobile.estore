﻿using System;

namespace Estore.Management.DataAccess.InvoiceLines
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class InvoiceLineInfoDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal LineId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int LineNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal CatelogId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int BrandTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CatelogName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal Price { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int Quantity { get; set; }
    }
}
