﻿using System.Linq;
using System.Collections.Generic;

namespace Estore.Management.DataAccess.Cities
{
    public static class CityDal
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly Dictionary<int, string> CityList;

        /// <summary>
        /// 
        /// </summary>
        static CityDal()
        {
            if (CityList == null)
            {
                CityList = new Dictionary<int, string>
                {
                    { 1, "Bhind" },
                    { 2, "Gohad" },
                    { 3, "Gormi" },
                    { 4, "Lahar" },
                    { 5, "Mehgaon" },
                    { 6, "Phoop" },
                    { 7, "Roan" },
                    { 8, "Umri" },
                    { 9, "Mou"},
                    { 10, "Machand"}
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string GetName(int cityId)
        {
            string cityName;
            CityList.TryGetValue(cityId, out cityName);
            return cityName ?? "Other";
        }

        /// <summary>
        /// 
        /// </summary>
        public static List<CityDto> Get()
        {
            var dtos = new List<CityDto>();
            foreach(var item in CityList)
            {
                dtos.Add(new CityDto(item.Key, item.Value));
            }
            return dtos.OrderBy(x => x.Name).ToList();
        }
    }
}
