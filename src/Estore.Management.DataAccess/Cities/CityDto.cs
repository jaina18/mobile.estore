﻿using System;

namespace Estore.Management.DataAccess.Cities
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CityDto
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public CityDto(int id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }
}
