﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Brands
{
    public static class BrandDal
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly Dictionary<int, string> BrandList;

        /// <summary>
        /// 
        /// </summary>
        static BrandDal()
        {
            if (BrandList == null)
            {
                BrandList = new Dictionary<int, string>
                {
                    { 1, "Itel" },
                    { 2, "Nokia" },
                    { 3, "Samsung" },
                    { 4, "RealMe" },
                    { 5, "Poco" },
                    { 6, "OnePlus"},
                    { 7, "MI"},
                    { 8, "IPhone"},
                    { 9, "Vivo"},
                    { 10, "Oppo"},
                    { 11, "Infinix"},
                    { 12, "Techno"},
                    { 13, "Jio"},
                    { 14, "Lava"},
                    { 15, "China" },
                    { 16, "Other" }
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string GetName(int brandId)
        {
            string name;
            BrandList.TryGetValue(brandId, out name);
            return name ?? "Other";
        }

        /// <summary>
        /// 
        /// </summary>
        public static List<BrandDto> Get()
        {
            var dtos = new List<BrandDto>();
            foreach (var item in BrandList)
            {
                dtos.Add(new BrandDto(item.Key, item.Value));
            }
            return dtos.OrderBy(x => x.Name).ToList();
        }
    }
}
