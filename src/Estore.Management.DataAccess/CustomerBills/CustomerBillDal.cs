﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.CustomerBills
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CustomerBillDal : ICustomerBillDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceDate"></param>
        /// <returns></returns>
        public async Task<List<CustomerBillDto>> FetchList(string invoiceDate)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.CustomerBills
                            where c.InvoiceDateTime == invoiceDate
                            select new CustomerBillDto()
                            {
                                Address = c.Address,
                                BaseAmount = c.BaseAmount,
                                BatteryNumber = c.BatteryNumber,
                                BillDateTime = c.BillDateTime,
                                CGST = c.CGST,
                                ChargerNumber = c.ChargerNumber,
                                Discount = c.Discount,
                                GrandTotal = c.GrandTotal,
                                Id = c.Id,
                                IMEINumber1 = c.IMEINumber1,
                                IMEINumber2 = c.IMEINumber2,
                                InvoiceDate = c.InvoiceDateTime,
                                InvoiceNumber = c.InvoiceNumber,
                                MobileNumber = c.MobileNumber,
                                ModelNumber = c.ModelNumber,
                                Name = c.Name,
                                SGST = c.SGST,
                                CatelogId = c.CatelogId,
                                CatelogDisplayName = c.Catelog.Name + " (" + c.Catelog.RAM + ", " + c.Catelog.Memory + ")",
                                BrandTypeId = c.Catelog.BrandTypeId
                            }).ToList();

                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public async Task<CustomerBillDto> Fetch(decimal Id)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.CustomerBills
                            where c.Id == Id
                            select c).SingleOrDefault();

                return new CustomerBillDto()
                {
                    Address = data.Address,
                    BaseAmount = data.BaseAmount,
                    BatteryNumber = data.BatteryNumber,
                    BillDateTime = data.BillDateTime,
                    CGST = data.CGST,
                    ChargerNumber = data.ChargerNumber,
                    Discount = data.Discount,
                    GrandTotal = data.GrandTotal,
                    Id = data.Id,
                    IMEINumber1 = data.IMEINumber1,
                    IMEINumber2 = data.IMEINumber2,
                    InvoiceDate = data.InvoiceDateTime,
                    InvoiceNumber = data.InvoiceNumber,
                    MobileNumber = data.MobileNumber,
                    ModelNumber = data.ModelNumber,
                    Name = data.Name,
                    SGST = data.SGST,
                    CatelogId = data.CatelogId,
                    BrandTypeId = data.Catelog?.BrandTypeId,
                    CatelogDisplayName = data.Catelog != null ? data.Catelog.Name + " (" + data.Catelog.RAM + ", " + data.Catelog.Memory + ")" : ""
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(CustomerBillDto dto)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var newItem = new CustomerBill()
                {
                    Address = dto.Address ?? string.Empty,
                    BaseAmount = dto.BaseAmount,
                    BatteryNumber = dto.BatteryNumber ?? string.Empty,
                    BillDateTime = dto.BillDateTime,
                    CGST = dto.CGST,
                    ChargerNumber = dto.ChargerNumber ?? string.Empty,
                    Discount = dto.Discount,
                    GrandTotal = dto.GrandTotal,
                    IMEINumber1 = dto.IMEINumber1 ?? string.Empty,
                    IMEINumber2 = dto.IMEINumber2 ?? string.Empty,
                    InvoiceDateTime = dto.InvoiceDate ?? DateTime.Now.ToString("dd-MM-yyyy"),
                    InvoiceNumber = dto.InvoiceNumber ?? string.Empty,
                    MobileNumber = dto.MobileNumber ?? string.Empty,
                    ModelNumber = dto.ModelNumber ?? string.Empty,
                    Name = dto.Name ?? string.Empty,
                    SGST = dto.SGST,
                    CatelogId = dto.CatelogId
                };

                ctx.CustomerBills.Add(newItem);
                ctx.SaveChanges();

                dto.Id = newItem.Id;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerBillId"></param>
        /// <returns></returns>
        public async Task Delete(decimal customerBillId)
        {
            using(var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.CustomerBills
                            where c.Id == customerBillId
                            select c).SingleOrDefault();

                ctx.CustomerBills.Remove(data);
                await ctx.SaveChangesAsync();
            }
        }
    }
}
