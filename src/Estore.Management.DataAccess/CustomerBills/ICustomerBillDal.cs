﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.CustomerBills
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICustomerBillDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceDate"></param>
        /// <returns></returns>
        Task<List<CustomerBillDto>> FetchList(string invoiceDate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        Task<CustomerBillDto> Fetch(decimal invoiceId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(CustomerBillDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerBillId"></param>
        Task Delete(decimal customerBillId);
    }
}
