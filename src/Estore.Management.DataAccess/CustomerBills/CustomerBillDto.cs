﻿using System;

namespace Estore.Management.DataAccess.CustomerBills
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CustomerBillDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? CatelogId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? BrandTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CatelogDisplayName { get; protected internal set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string InvoiceNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string MobileNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string ModelNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string IMEINumber1 { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string IMEINumber2 { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string BatteryNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ChargerNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal BaseAmount { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal? Discount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal SGST { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal CGST { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal GrandTotal { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string InvoiceDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime BillDateTime { get; set; }
    }
}
