﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Catelogs
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CatelogDal : ICatelogDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandTypeId"></param>
        /// <returns></returns>
        public async Task<List<CatelogInfoDto>> FetchList(int? brandTypeId = null)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var query = from c in ctx.Catelogs.AsQueryable()
                            orderby c.BrandTypeId, c.Name
                            select new
                            {
                                c.BrandTypeId,
                                c.CatelogId,
                                c.CreatedDate,
                                c.Memory,
                                c.RAM,
                                c.Name,
                                c.Active,
                                TotalPOQuantity = c.PurchaseOrders.Count() > 0 ? c.PurchaseOrders.Sum(x => x.Quantity) : 0,
                                TotalInvoiceSaleQuantity = c.InvoiceLines.Count() > 0 ? c.InvoiceLines.Where(x => x.CatelogId == c.CatelogId).Sum(x => x.Quantity) : 0,
                                TotalCustomerBillSaleQuantity = c.CustomerBills.Count() > 0 ? c.CustomerBills.Where(x => x.CatelogId == c.CatelogId).Sum(x => 1) : 0,
                                LastOrderDateTime = c.PurchaseOrders.FirstOrDefault() != null ? c.PurchaseOrders.OrderByDescending(x => x.OrderDate).FirstOrDefault().OrderDate : DateTime.MinValue,
                                LastOrderQuantity = c.PurchaseOrders.FirstOrDefault() != null ? c.PurchaseOrders.OrderByDescending(x => x.OrderDate).FirstOrDefault().Quantity : 0
                            };

                if (brandTypeId != null)
                {
                    query = query.Where(x => x.BrandTypeId == brandTypeId);
                }

                var data = (from c in query
                            select new CatelogInfoDto()
                            {
                                BrandTypeId = c.BrandTypeId,
                                CatelogId = c.CatelogId,
                                CreatedDate = c.CreatedDate,
                                Memory = c.Memory,
                                RAM = c.RAM,
                                Name = c.Name,
                                Active = c.Active ?? false,
                                Quantity = c.TotalPOQuantity - (c.TotalInvoiceSaleQuantity + c.TotalCustomerBillSaleQuantity),
                                LastOrderDateTime = c.LastOrderDateTime,
                                LastOrderQuantity = c.LastOrderQuantity
                            }).ToList();
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        public async Task<List<SelectedDateCatelogInfoDto>> FetchList(DateTime dateTime, decimal catelogId)
        {
            await Task.FromResult(1);
            var date = dateTime.Date;
            using(var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.InvoiceLines
                            where c.Invoice.InvoiceDate == date && c.CatelogId == catelogId
                            select new SelectedDateCatelogInfoDto()
                            {
                                CityId = c.Invoice.Retailer.CityId,
                                Name = c.Invoice.Retailer.Name + " " + c.Invoice.Retailer.Address,
                                InvoiceNumber = c.Invoice.InvoiceNumber,
                                Quantity = c.Quantity
                            }).ToList();
                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        public async Task<CatelogDto> Get(decimal catelogId)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Catelogs
                            where c.CatelogId == catelogId
                            select new CatelogDto()
                            {
                                BrandTypeId = c.BrandTypeId,
                                CatelogId = c.CatelogId,
                                Active = c.Active ?? false,
                                Memory = c.Memory,
                                Name = c.Name,
                                RAM = c.RAM
                            }).SingleOrDefault();

                return data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(CatelogDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = new Catelog()
                {
                    BrandTypeId = dto.BrandTypeId,
                    CatelogId = dto.CatelogId,
                    CreatedDate = dto.CreatedDate,
                    Memory = dto.Memory,
                    RAM = dto.RAM,
                    Name = dto.Name,
                    Active = true
                };

                ctx.Catelogs.Add(data);
                await ctx.SaveChangesAsync();

                dto.CatelogId = data.CatelogId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(CatelogDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Catelogs
                            where c.CatelogId == dto.CatelogId
                            select c).SingleOrDefault();

                data.Name = dto.Name;
                data.RAM = dto.RAM;
                data.Memory = dto.Memory;
                data.BrandTypeId = dto.BrandTypeId;

                await ctx.SaveChangesAsync();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        public async Task UpdateStatus(decimal catelogId)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Catelogs
                            where c.CatelogId == catelogId
                            select c).SingleOrDefault();

                data.Active = !data.Active;
                await ctx.SaveChangesAsync();
            }
        }
    }
}
