﻿using System;

namespace Estore.Management.DataAccess.Catelogs
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CatelogDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal CatelogId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int BrandTypeId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Active { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string RAM { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Memory { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }
}
 