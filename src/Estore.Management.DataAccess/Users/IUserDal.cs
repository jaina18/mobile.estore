﻿using System;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Users
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUserDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        void Insert(UserDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<UserDto> Fetch(string emailAddress, string password);
    }
}
