﻿using System;

namespace Estore.Management.DataAccess.Invoices
{
    public sealed class InvoiceDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal InvoiceId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string InvoiceNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public System.DateTime InvoiceDate { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool Archived { get; set; }

    }
}
