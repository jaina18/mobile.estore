﻿using Estore.Management.DataAccess.InvoiceLines;
using System.Collections.Generic;

namespace Estore.Management.DataAccess.Invoices
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class InvoiceInfoDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal InvoiceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal RetailerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.DateTime InvoiceDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<InvoiceLineInfoDto> Lines { get; set; }
    }
}
