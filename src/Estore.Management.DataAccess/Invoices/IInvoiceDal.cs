﻿using System;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Invoices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IInvoiceDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        Task<InvoiceInfoDto> Get(decimal invoiceId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(InvoiceDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(InvoiceDto dto);
    }
}
