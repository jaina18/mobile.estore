﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.PurchaseOrders
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class PurchaseOrderDal : IPurchaseOrderDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catelogId"></param>
        /// <returns></returns>
        public async Task<List<PurchaseOrderDto>> FetchList(decimal catelogId)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.PurchaseOrders
                            where c.CatelogId == catelogId
                            select new PurchaseOrderDto()
                            {
                                Archived = c.Archived,
                                CatelogId = c.CatelogId,
                                OrderDate = c.OrderDate,
                                PurchaseOrderId = c.PurchaseOrderId,
                                Quantity = c.Quantity,
                                Price = c.Price
                            }).ToList();

                return data.OrderByDescending(x => x.OrderDate).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="poDate"></param>
        /// <returns></returns>
        public async Task<List<PurchaseOrderDto>> FetchList(DateTime poDate)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var date = poDate.Date;
                var data = (from c in ctx.PurchaseOrders
                            where c.OrderDate == date
                            select new PurchaseOrderDto()
                            {
                                Archived = c.Archived,
                                BrandTypeId = c.Catelog.BrandTypeId,
                                CatelogId = c.CatelogId,
                                CatelogDisplayName = c.Catelog.Name + " (" + c.Catelog.RAM + ", " + c.Catelog.Memory + ")",
                                OrderDate = c.OrderDate,
                                PurchaseOrderId = c.PurchaseOrderId,
                                Quantity = c.Quantity,
                                Price = c.Price
                            }).ToList();

                return data.OrderByDescending(x => x.CatelogDisplayName).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(PurchaseOrderDto dto)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = new PurchaseOrder()
                {
                    CatelogId = dto.CatelogId,
                    Archived = dto.Archived,
                    OrderDate = dto.OrderDate,
                    Quantity = dto.Quantity,
                    Price = dto.Price
                };

                ctx.PurchaseOrders.Add(data);
                await ctx.SaveChangesAsync();

                dto.PurchaseOrderId = data.PurchaseOrderId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="purchaseOrderId"></param>
        public async Task Remove(decimal purchaseOrderId)
        {
            using (var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.PurchaseOrders
                            where c.PurchaseOrderId == purchaseOrderId
                            select c).SingleOrDefault();

                ctx.PurchaseOrders.Remove(data);
                await ctx.SaveChangesAsync();
            }
        }
    }
}
