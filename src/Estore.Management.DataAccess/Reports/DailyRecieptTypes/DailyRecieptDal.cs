﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class DailyRecieptDal : IDailyRecieptDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public async Task<List<DailyRecieptDto>> FetchList(DateTime date)
        {
            await Task.FromResult(1);
            using(var ctx = new MobileEStoreRepository())
            {
                var data = (from c in ctx.Reciepts
                            where c.RecieptDate == date
                            select new DailyRecieptDto()
                            {
                                RecieptId = c.RecieptId,
                                Amount = c.Amount,
                                RecieptNumber = c.RecieptNumber,
                                RetailerId = c.RetailerId,
                                RetailerName = c.Retailer.Name + " " + c.Retailer.Address
                            }).ToList();

                return data;
            }
        }
    }
}
