﻿using System;
using System.Collections.Generic;

namespace Estore.Management.DataAccess.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class StockDto
    {
        /// <summary>
        /// 
        /// </summary>
        public DateTime StockDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<StockCatelogDto> Catelogs { get; set; }
    }
}
