﻿using Estore.Management.DataAccess.Brands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class StockDal : IStockDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="brandTypeId"></param>
        /// <returns></returns>
        public async Task<List<StockDto>> FetchList(int year, int month, int? brandTypeId = null)
        {
            var dtos = await GetAllDatesStock(year, month, brandTypeId);
            return dtos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        private async Task<List<StockDto>> GetAllDatesStock(int year, int month, int? brandTypeId = null)
        {
            var dtos = new List<StockDto>();
            var dates = await GetDates(year, month);
            foreach (var date in dates)
            {
                if (date <= DateTime.Now.Date)
                {
                    var dto = await GetCatelog(date, brandTypeId);
                    dtos.Add(dto);
                }
            }
            return dtos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        private async Task<StockDto> GetCatelog(DateTime dateTime, int? brandTypeId = null)
        {
            await Task.FromResult(1);
            var date = dateTime.Date;
            using (var ctx = new MobileEStoreRepository())
            {
                var catelogs = (from c in ctx.Catelogs
                                where brandTypeId == null ? c.Active == true : c.Active == true && c.BrandTypeId == brandTypeId
                                orderby c.BrandTypeId, c.Name
                                select new StockCatelogDto()
                                {
                                    BrandTypeId = c.BrandTypeId,
                                    Name =  c.Name + " (" + c.RAM + ", " + c.Memory + ") ",
                                    PurchasedQuantity = c.PurchaseOrders.Where(x => x.OrderDate <= date && x.CatelogId == c.CatelogId).Sum(x => x.Quantity),
                                    SaleQuantity = c.InvoiceLines.Where(x => x.Invoice.InvoiceDate <= date && x.CatelogId == c.CatelogId).Sum(x => x.Quantity)
                                }).ToList();
                var dto = new StockDto()
                {
                    Catelogs = catelogs,
                    StockDate = dateTime
                };
                return dto;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        private async Task<List<DateTime>> GetDates(int year, int month)
        {
            await Task.FromResult(1);
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                             .Select(day => new DateTime(year, month, day)).ToList();
        }
    }
}
