﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class DailyCatelogSaleDal : IDailyCatelogSaleDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedDate"></param>
        /// <returns></returns>
        public async Task<List<DailyCatelogSaleDto>> FetchList(DateTime selectedDate)
        {
            await Task.FromResult(1);
            using (var ctx = new MobileEStoreRepository())
            {
                var query = (from c in ctx.Catelogs
                            where c.Active == true
                            orderby c.BrandTypeId, c.Name
                            select new
                            {
                                c.BrandTypeId,
                                c.CatelogId,
                                CatelogName = c.Name + " (" + c.RAM + ", " + c.Memory + ")",
                                Quantity = c.InvoiceLines.Where(x => x.Invoice.InvoiceDate == selectedDate && x.CatelogId == c.CatelogId).Sum(z => z.Quantity)
                            });

                var data = (from c in query
                            where c.Quantity > 0
                            select new DailyCatelogSaleDto()
                            {
                                BrandTypeId = c.BrandTypeId,
                                CatelogId = c.CatelogId,
                                CatelogName = c.CatelogName,
                                Quantity = c.Quantity
                            }).ToList();

                return data;
            }
        }
    }
}
