﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Estore.Management.DataAccess.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDailyCatelogSaleDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedDate"></param>
        Task<List<DailyCatelogSaleDto>> FetchList(DateTime selectedDate);
    }
}
